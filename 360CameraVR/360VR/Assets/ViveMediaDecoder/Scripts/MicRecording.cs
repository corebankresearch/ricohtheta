﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicRecording : MonoBehaviour {
	private void Start() {
		AudioSource aud = GetComponent<AudioSource> ();
		foreach (string mic in Microphone.devices) {
			Debug.Log (mic);
		}
		string micToUse = Microphone.devices[0].ToString();
		aud.clip = Microphone.Start (micToUse, true, 10, 44100);

		aud.Play ();
	}
}
