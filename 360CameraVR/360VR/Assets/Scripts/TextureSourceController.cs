﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HTC.UnityPlugin.Multimedia {
    public class TextureSourceController : ImageSourceController {
        private int currentIndex = 0;
        [SerializeField]
        public Texture2D[] textures;

        public override void initFileSeeker() {

            oriScale = transform.localScale;

            isInitialized = true;

            onInitComplete.Invoke();
        }

        protected override IEnumerator loadImageCoroutine(string imagePath) {
            if (currentIndex > textures.Length - 1 || textures.Length <= 0) {
                yield break;
            }
            texture = textures[currentIndex];
            GetComponent<MeshRenderer>().material.mainTexture = texture;
            if (isAdaptToResolution) {
                adaptResolution();
            }

            yield return null;
        }

        public override void nextImage() {
            if (!isInitialized) {
                Debug.Log(LOG_TAG + " not initialized.");
                return;
            }

            currentIndex = currentIndex + 1 < textures.Length ? currentIndex + 1 : 0;
            onChangeImage.Invoke();
        }

        public override void prevImage() {
            if (!isInitialized) {
                Debug.Log(LOG_TAG + " not initialized.");
                return;
            }

            currentIndex = currentIndex - 1 >= 0 ? currentIndex - 1 : textures.Length - 1;
            onChangeImage.Invoke();
        }

        public override void loadImage() {
            if (!isInitialized) {
                Debug.Log(LOG_TAG + " not initialized.");
                return;
            }

            StartCoroutine(loadImageCoroutine(""));
        }
    }
}