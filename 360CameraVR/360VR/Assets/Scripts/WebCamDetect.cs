﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebCamDetect : MonoBehaviour
{

	void Start()
	{
		//add regedit entry DevicePath with value foo:bar at:
		//Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Classes\CLSID\{860BB310-5D01-11D0-BD3B-00A0C911CE86}\Instance\{44ADD235-FE8A-42D8-8B18-CF575554AAB1}
		WebCamDevice[] devices = WebCamTexture.devices;
		Debug.Log("Number of web cams connected: " + devices.Length);

		for (int i=0; i < devices.Length; i++)
		{
			Debug.Log(i + " " + devices[i].name);
		}

		Renderer rend = this.GetComponentInChildren<Renderer>();

		WebCamTexture mycam = new WebCamTexture();
		string camName = devices[5].name;
		Debug.Log("The webcam name is " + camName);
		mycam.deviceName = camName;
		rend.material.mainTexture = mycam;

		mycam.Play();
	}
}