﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ControllerCycle : MonoBehaviour {
    private SteamVR_TrackedObject trackedObject;
    private SteamVR_Controller.Device inputDevice { get { return SteamVR_Controller.Input((int)trackedObject.index); } }
    private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;

    public UnityEvent triggerButtonEvent;
    
    // Use this for initialization
    void Start () {
        trackedObject = GetComponent<SteamVR_TrackedObject>();
	}
	
	// Update is called once per frame
	void Update () {
        if (inputDevice == null)
            return;

        if (inputDevice.GetPressUp(triggerButton))
        {
            triggerButtonEvent.Invoke();
        }
	}
}
