﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CycleVideos : MonoBehaviour {
	public HTC.UnityPlugin.Multimedia.VideoSourceController controller;

	void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			controller.prevVideo();
		}
		else if (Input.GetKeyDown (KeyCode.RightArrow)) {
			controller.nextVideo();
		}
	}
}
