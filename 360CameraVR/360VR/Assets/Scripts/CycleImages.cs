﻿using UnityEngine;
using UnityEngine.VR;

public class CycleImages : MonoBehaviour {
    [SerializeField]
    private ImageController imageController;

    [SerializeField]
    private SteamVR_TrackedController rightSteamController;

    private void Start()
    {
        rightSteamController.TriggerClicked += TriggerPressed;
    }

    void Update () {
		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
            imageController.PreviousImage();
		}
		else if (Input.GetKeyDown (KeyCode.RightArrow)) {
            imageController.NextImage ();
		}
	}

    private void TriggerPressed(object sender, ClickedEventArgs e)
    {
        imageController.NextImage();
    }
}
