﻿/* ImageController.cs, class for loading and displaying images */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class ImageController : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer meshRenderer;

    private Object[] textures;
    private Texture2D currentTexture;

    private bool isInitialized = false;

    private int currentIndex = 0;
    private int lastIndex;

    // Start is called before the first frame update
    void Start()
    {
        LoadImages();
        SetImageToMeshRenderer();
    }

    private void LoadImages()
    {
        textures = Resources.LoadAll("Textures", typeof(Texture2D));
        CalculateLastIndex();
        isInitialized = true;
    }

    private void SetImageToMeshRenderer()
    {
        currentTexture = new Texture2D(1, 1);
        currentTexture.filterMode = FilterMode.Trilinear;
        currentTexture.Apply();
        currentTexture = (Texture2D)textures[currentIndex];
        meshRenderer.material.mainTexture = currentTexture;
    }

    public void NextImage()
    {
        if (!isInitialized)
        {
            Debug.LogError("Not initialized all images.");
        }
        if (currentIndex < lastIndex)
        {
            currentIndex++;
        }
        else
        {
            currentIndex = 0;
        }
        SetImageToMeshRenderer();
    }

    public void PreviousImage()
    {
        if (!isInitialized)
        {
            Debug.LogError("Not Initialized all images.");
            return;
        }
        if (currentIndex > 0)
        {
            currentIndex--;
        }
        else
        {
            currentIndex = lastIndex;
        }
        SetImageToMeshRenderer();
    }

    private void CalculateLastIndex()
    {
        lastIndex = textures.Length - 1;
    }
}
